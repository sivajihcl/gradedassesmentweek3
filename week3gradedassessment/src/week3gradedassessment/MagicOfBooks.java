package week3gradedassessment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagicOfBooks {
	private Map<Integer, Book> books;
	
	MagicOfBooks() {
		
		this.books = new HashMap<>();
		}
	
	public boolean addBook(int id, Book book) {
		books.put(id, book);
		return true;
		}
	
	public boolean deleteBook(int id) {
		return books.remove(id) !=null;
		}
	public void updateBook(int id, Book book) {
		books.put(id, book);
		}
	public void display() {
		System.out.println(books);
		}
	public void countOfBooks() {
		System.out.println(books.size());
		}
	public void displayByGenre(String genre) {
		Collection<Book> books2 = books.values();
		books2.stream().filter(book -> book.getGenre().equalsIgnoreCase(genre)).forEach(System.out::println);
		}
	
	public void displayInOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
			}
		Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? 1 : -1);
		System.out.println(books2);
		}
	public void displayInDescOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
			}
		Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? -1 : 1);
		System.out.println(books2);
		}
	public void displayInSoldOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
			}
		Collections.sort(books2, (book1, book2) -> book1.getNoOfCopyesSold() > book2.getNoOfCopyesSold() ? -1 : 1);
		System.out.println(books2);
		}
	public static void main(String[] args) {
		MagicOfBooks magic = new MagicOfBooks();
		magic.addBook(1, new Book("A", 250, "Fantasy", 4));
		magic.addBook(2, new Book("B", 150, "Fiction", 5));
		magic.addBook(3, new Book("C", 500, "Biography", 2));
		magic.addBook(4, new Book("D", 200, "Novella", 5));
		magic.addBook(5, new Book("E", 320, "Mystery", 4));
		magic.addBook(6, new Book("F", 710, "romance", 6));
		
		magic.display();
		magic.displayByGenre("Fantasy");
		magic.displayInDescOrder();
		magic.displayInOrder();
		magic.displayInSoldOrder();
		magic.deleteBook(2);
		magic.display();
		magic.display();
		magic.updateBook(2, new Book("G",500,"Drama",5));
		magic.display();
		magic.countOfBooks();
		
		magic.deleteBook(4);
		magic.display();
		
	
	}
	
} 